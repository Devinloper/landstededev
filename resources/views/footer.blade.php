{{-- Footer element to be re-used, the $footer and $colours values have to be passed with the include. --}}
<section class="page-footer">
    <div class="container-fluid"
         style="background-color: {{ $colours->background }}; color: {{ $colours->text }}">
        <div class="container">
            <div class="row align-items-center justify-content-center">
                <div class="col-4 text-center">
                    @isset($footer->links)
                        @foreach($footer->links as $link)
                            <a href="{{ $link->link }}" class="btn"
                               style="margin: 5px; width: 100%; word-wrap: break-word; background-color: {{ $link->colour_button }}; color: {{ $link->colour_label }}">{{ $link->label }}</a>
                            <br>
                        @endforeach
                    @endisset
                </div>
                <div class="col-8" style="border-left: 2px solid {{ $colours->text }}">
                    @isset($footer)
                        <h2 class="title">{{ $footer->title }}</h2>
                        <p>{!! nl2br(e($footer->content ?? '')) !!}</p>
                    @endisset
                </div>
            </div>
        </div>
    </div>
</section>
