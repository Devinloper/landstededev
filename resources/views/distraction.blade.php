{{-- Distraction element to be re-used, the $distraction and $colours values have to be passed with the include. --}}
<section>
    <div class="container-fluid"
         style="background-color: {{ $colours[1]->background }}; color: {{ $colours[1]->text }}; padding: 20px 0">
        <div class="container">
            <div class="row align-items-center justify-content-center">
                <div class="col-12 text-center">
                    <p>{!! nl2br(e($distraction->text ?? '')) !!}</p>
                </div>
                <div class="col text-center">
                    <a href="{{ $distraction->link ?? '#' }}" class="btn"
                       style="background-color: {{ $colours[2]->background }}; color: {{ $colours[2]->text }}">Meld Je
                        Aan!</a>
                </div>
            </div>
        </div>
    </div>
</section>
