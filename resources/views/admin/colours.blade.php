@extends('admin.layouts.app', ['titlePage' => 'Change Colours'])


@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    @isset($colours)
                        @foreach($colours as $colour)
                            <div class="col-md-6">
                                <div class="card">
                                    <div class="header">
                                        <h4 class="title">{{ $colour->name }}</h4>
                                    </div>
                                    <div class="content">
                                        <form method="POST" action="{{ route('colour.update', $colour) }}">
                                            @csrf
                                            @method('PUT')
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="background{{ $colour->id }}" class="form-label">
                                                            Background Colour</label>
                                                        <input id="background{{ $colour->id }}" type="color"
                                                               class="form-control form-control-color"
                                                               name="backgroundColour"
                                                               required
                                                               value="{{ $colour->background ?? old('backgroundColour') }}">

                                                        <label for="text{{ $colour->id }}" class="form-label">
                                                            Text Colour</label>
                                                        <input id="text{{ $colour->id }}" type="color"
                                                               class="form-control form-control-color" name="textColour"
                                                               required
                                                               value="{{ $colour->text ?? old('textColour') }}">
                                                    </div>
                                                </div>
                                            </div>
                                            <input type="submit" class="btn btn-info btn-fill pull-right"
                                                   value="Save">
                                            <div class="clearfix"></div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @endisset
                </div>
                <div class="col-md-6">
                    @isset($colours)
                        @foreach($colours as $colour)
                            <div class="text-center align-content-center justify-content-center" style="background-color: {{ $colour->background }}; color: {{ $colour->text }}; height: 100px;">
                                <p>{{ $colour->name }}</p>
                            </div>
                        @endforeach
                    @endisset
                </div>
            </div>
        </div>
    </div>
@endsection

