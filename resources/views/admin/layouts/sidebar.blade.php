<div class="sidebar" data-color="purple" data-image="{{ asset('admin') }}/img/sidebar-5.jpg">

    <!--

        You can change the color of the sidebar using: data-color="blue | azure | green | orange | red | purple"
        You can also add an image using data-image tag

    -->

    <div class="sidebar-wrapper">
        <div class="logo">
            <a href="#" class="simple-text">
                Admin Panel
            </a>
        </div>

        <ul class="nav">
            <li class="{{ Route::currentRouteName() == 'admin.video' ? ' active' : '' }}">
                <a href="{{ route('admin.video') }}">
                    <i class="pe-7s-video"></i>
                    <p>Uitgelichte Video</p>
                </a>
            </li>
            <li class="{{ Route::currentRouteName() == 'admin.distraction' ? ' active' : '' }}">
                <a href="{{ route('admin.distraction') }}">
                    <i class="pe-7s-link"></i>
                    <p>Meld Je Aan</p>
                </a>
            </li>
            <li class="{{ Route::currentRouteName() == 'admin.news' ? ' active' : '' }}">
                <a href="{{ route('admin.news') }}">
                    <i class="pe-7s-news-paper"></i>
                    <p>Nieuws</p>
                </a>
            </li>
            <li class="{{ Route::currentRouteName() == 'admin.portfolio' ? ' active' : '' }}">
                <a href="{{ route('admin.portfolio') }}">
                    <i class="pe-7s-portfolio"></i>
                    <p>Portfolio</p>
                </a>
            </li>
            <li class="{{ Route::currentRouteName() == 'admin.languages' ? ' active' : '' }}">
                <a href="{{ route('admin.languages') }}">
                    <i class="pe-7s-plugin"></i>
                    <p>Languages</p>
                </a>
            </li>
            <li class="{{ Route::currentRouteName() == 'admin.footer' ? ' active' : '' }}">
                <a href="{{ route('admin.footer') }}">
                    <i class="pe-7s-bottom-arrow"></i>
                    <p>Footer</p>
                </a>
            </li>
            <li class="{{ Route::currentRouteName() == 'admin.colours' ? ' active' : '' }}">
                <a href="{{ route('admin.colours') }}">
                    <i class="pe-7s-eyedropper"></i>
                    <p>Change Colours</p>
                </a>
            </li>
            <li class="{{ Route::currentRouteName() == 'admin.user' ? ' active' : '' }}">
                <a href="{{ route('admin.user') }}">
                    <i class="pe-7s-lock"></i>
                    <p>Change Password</p>
                </a>
            </li>
        </ul>
    </div>
</div>
