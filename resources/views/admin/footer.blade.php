@extends('admin.layouts.app', ['titlePage' => 'Edit Footer'])


@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Edit Footer Links</h4>
                            </div>
                            <div class="content">
                                @isset($footer->links)
                                    @foreach($footer->links as $link)
                                        <div class="card">
                                            <div class="content">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <form id="linkForm{{ $link->id }}"
                                                              action="{{ route('link.update', $link) }}"
                                                              method="POST">
                                                            @csrf
                                                            @method('PUT')
                                                            <label for="linkLabel">Link Label</label>
                                                            <input type="text" id="linkLabel" class="form-control"
                                                                   name="linkLabel"
                                                                   value="{{ $link->label ?? old('linkLabel') }}">
                                                            <label for="linkLink">Link Link</label>
                                                            <input type="text" id="linkLink" class="form-control"
                                                                   name="linkLink"
                                                                   value="{{ $link->link ?? old('linkLink') }}">
                                                            <label for="colourLabel" class="form-label">
                                                                Label Colour</label>
                                                            <input id="colourLabel" type="color"
                                                                   class="form-control form-control-color"
                                                                   name="colourLabel"
                                                                   required
                                                                   value="{{ $link->colour_label ?? old('colourLabel') }}">
                                                            <label for="colourButton" class="form-label">
                                                                Button Colour</label>
                                                            <input id="colourButton" type="color"
                                                                   class="form-control form-control-color"
                                                                   name="colourButton"
                                                                   required
                                                                   value="{{ $link->colour_button ?? old('colourBackground') }}">
                                                            <br>
                                                            <div class="col-md-10 col-xs-7">
                                                                <button class="btn btn-primary btn-sm saveChanges"
                                                                        type="submit"><span
                                                                        class="material-icons"
                                                                        style="font-size: 20px">save</span>
                                                                </button>
                                                            </div>
                                                        </form>
                                                        <div class="col-md-2 col-xs-5">
                                                            <form action="{{ route('link.destroy', $link) }}"
                                                                  method="POST">
                                                                @csrf
                                                                @method('DELETE')
                                                                <button type="submit"
                                                                        class="btn btn-danger btn-sm deleteLink pull-right"><span
                                                                        class="material-icons"
                                                                        style="font-size: 20px">delete_forever</span>
                                                                </button>
                                                            </form>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    @endforeach
                                @endisset
                                <div class="card hidden" id="newLinkFormCard">
                                    <div class="content">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <form id="newLinkForm" action="{{ route('link.store') }}"
                                                      method="POST">
                                                    @csrf
                                                    @method('POST')
                                                    <label for="newLinkLabel">New Link Label</label>
                                                    <input type="text" id="newLinkLabel" class="form-control"
                                                           name="linkLabel">
                                                    <br>
                                                    <label for="newLinkLink">New Link</label>
                                                    <input type="text" id="newLinkLink" class="form-control"
                                                           name="linkLink">
                                                    <label for="newColourLabel" class="form-label">
                                                        Label Colour</label>
                                                    <input id="newColourLabel" type="color"
                                                           class="form-control form-control-color"
                                                           name="colourLabel"
                                                           required>
                                                    <label for="newColourButton" class="form-label">
                                                        Button Colour</label>
                                                    <input id="newColourButton" type="color"
                                                           class="form-control form-control-color"
                                                           name="colourButton"
                                                           required>
                                                    <br>
                                                    <div class="col-md-10 col-xs-7">
                                                        <button class="btn btn-primary btn-sm" type="submit"><span
                                                                class="material-icons"
                                                                style="font-size: 20px">save</span>
                                                        </button>
                                                    </div>
                                                </form>
                                                <div class="col-md-2 col-xs-5">
                                                    <button onclick="closeNewForm();"
                                                            class="btn btn-danger btn-sm pull-right"><span
                                                            class="material-icons"
                                                            style="font-size: 20px">delete_forever</span>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="content text-center">
                                        <button onclick="openNewForm()" id="newLink" class="btn btn-success"><span
                                                class="material-icons"
                                                style="font-size: 20px">add_circle_outline</span>New
                                            Link
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Edit Footer Content</h4>
                            </div>
                            <div class="content">
                                <form method="POST" action="{{ route('footer.update', $footer) }}">
                                    @csrf
                                    @method('PUT')
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="footerTitle" class="form-label">
                                                    Footer Title</label>
                                                <input id="footerTitle" type="text"
                                                       class="form-control"
                                                       name="footerTitle"
                                                       required
                                                       value="{{ $footer->title ?? old('footerTitle') }}">

                                                <br>

                                                <label for="footerContent" class="form-label">
                                                    Footer Content</label>
                                                <textarea style="width: 100%;" id="footerContent" class="form-control"
                                                          name="footerContent" cols="30" rows="10"
                                                          required>{{ $footer->content ?? old('textColour') }}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <input type="submit" class="btn btn-info btn-fill pull-right"
                                           value="Save">
                                    <div class="clearfix"></div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script type="text/javascript">
        function closeNewForm() {
            $('#newLinkFormCard').addClass('hidden');
            $('#newLinkLabel').val(null);
            $('#newLinkLink').val(null);
            $('#newColourLabel').val(null);
            $('#newColourButton').val(null);
            $('#newLink').attr('disabled', false);
            $('.saveChanges').attr('disabled', false);
            $('.deleteLink').attr('disabled', false);
        }

        function openNewForm() {
            $('#newLinkFormCard').removeClass('hidden');
            $('#newLink').attr('disabled', true);
            $('.saveChanges').attr('disabled', true);
            $('.deleteLink').attr('disabled', true);
        }
    </script>
@endpush
