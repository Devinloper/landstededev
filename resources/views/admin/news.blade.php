@extends('admin.layouts.app', ['titlePage' => 'Articles'])


@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    @isset($articles)
                        @foreach($articles as $article)
                            <div class="card">
                                <div class="header text-center">
                                    <div>
                                        <div class="img-fluid"
                                             style='background: url("{{ asset('storage/articles/' . $article->image_path) }}") no-repeat center center; background-size: cover; padding-top: 56.25%;'></div>
                                    </div>
                                    <br>
                                    <h4 class="title">{{ $article->name }}</h4>
                                </div>
                                <div class="content">
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="form-group text-center">
                                                <button id="editArticle" onclick="fetch_article({{ $article->id }})"
                                                        class="btn btn-primary editArticle col-6"
                                                        type="button">
                                                            <span class="material-icons"
                                                                  style="font-size: 20px">edit</span>Edit Article
                                                </button>
                                                <form class="col-6" method="POST"
                                                      action="{{ route('article.destroy', $article) }}">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit"
                                                            class="btn btn-danger"><span
                                                            class="material-icons" style="font-size: 20px">delete_forever</span>Delete
                                                        Article
                                                    </button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @endisset
                </div>
            </div>
            <div id="articleForm" class="row hidden">
                <div class="col-md-12">
                    <div class="card col-md-12">
                        <div class="header form-header text-center">
                            <div class="pull-right close" id="closeArticleForm"
                                 style="color: darkred; font-size: 22px"><i
                                    class="pe-7s-close-circle"></i>
                            </div>
                            <h4 class="title">Article Form</h4>
                        </div>
                        <div class="content text-center">
                            <form id="articleFormElement" method="POST" enctype="multipart/form-data">
                                @csrf
                                <input id="articleFormMethod" type="hidden" name="_method" value="POST">
                                <div class="col-md-10 col-md-offset-1">
                                    <div class="form-group" style="margin-bottom: 30px">
                                        <input style="width: 100%;" required type="file" class="btn col-md-12"
                                               name="articleImage"
                                               id="articleImage">
                                    </div>
                                    <br>
                                    <div class="form-group">
                                        <label for="articleName">Article Name</label>
                                        <input required id="articleName" type="text" class="form-control"
                                               name="articleName">
                                        <br>
                                        <label for="articleContent">Article Content</label>
                                        <textarea required id="articleContent" type="text" class="form-control"
                                                  name="articleContent" cols="30" rows="10"></textarea>
                                        <input type="hidden" name="isEdit" id="isEdit">
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" id="submitButton" class="btn btn-primary">Save Article
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="content">
                            <div class="row text-center">
                                <button id="newArticle" class="btn btn-success newArticle"
                                        type="button">
                                    <span class="material-icons"
                                          style="font-size: 20px">add_circle_outline</span>New Article
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script type="text/javascript">
        $(document).ready(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        });

        $('#closeArticleForm').on('click', () => {
            $('#articleForm').addClass('hidden');

            $('#articleName').val(null);
            $('#articleContent').val(null);
            $('#articleImagePreview').remove();
            $('#isEdit').val(false);

            $('#articleFormElement').attr('action', null);
            $('#articleFormMethod').val('POST');

            $('#articleImage').attr('required');
            $('.editArticle').attr('disabled', false);
            $('.newArticle').attr('disabled', false);
        })

        $('#newArticle').on('click', () => {
            $('#articleForm').removeClass('hidden');

            $('#articleFormElement').attr('action', '/panel/news/store');
            $('#articleFormMethod').val('POST');

            $('#isEdit').val(false);
            $('.editArticle').attr('disabled', true);
            $("html, body").animate({scrollTop: $('#articleForm').offset().top}, "slow");
        })

        function showEditForm(article) {
            $('.form-header').append('<img id="articleImagePreview" class="img-thumbnail" src="" alt="Article Image">');
            $('#articleImagePreview').attr("src", "{{ asset('storage/articles/') }}/" + article.image_path);

            $('.newArticle').attr('disabled', true);
            $('.editArticle').attr('disabled', true);

            $('#articleName').val(article.name);
            $('#articleContent').val(article.content);
            $('#isEdit').val(true);
            $('#articleFormElement').attr('action', '/panel/news/' + article.id);
            $('#articleFormMethod').val('PUT');

            $('#articleImage').removeAttr('required');

            $('#articleForm').removeClass('hidden');
            $("html, body").animate({scrollTop: $('#articleForm').offset().top}, "slow");
        }

        function fetch_article(articleId) {
            $.ajax({
                url: '/panel/news/' + articleId,
                type: 'GET',
            })
                .done(function (results) {
                    showEditForm(results[0]);
                })
                .fail(function (XMLHttpRequest, textStatus, errorThrown) {
                    console.error("Error: " + errorThrown);
                    $.notify({
                        icon: 'pe-7s-close-circle',
                        message: "AJAX error " + textStatus + "! Please contact the application administrator."
                    }, {
                        type: 'danger',
                        timer: 4000
                    });
                });
        }
    </script>
@endpush

