@extends('admin.layouts.app', ['titlePage' => 'Languages'])


@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    @isset($languages)
                        @foreach($languages as $language)
                            <div class="col-md-2">
                                <div class="card">
                                    <div class="header text-center">
                                        @if($language->icon_name != null)
                                            <h4 class="title"><i
                                                    class="fab fa-{{ Str::lower($language->icon_name) }}"></i>&nbsp;{{ $language->name }}
                                            </h4>
                                        @else
                                            <h4 class="title"><i
                                                    class="fab fa-{{ Str::lower($language->name) }}"></i>&nbsp;{{ $language->name }}
                                            </h4>
                                        @endif
                                    </div>
                                    <div class="content">
                                        <div class="col-md-10 col-xs-7">
                                            <button class="btn btn-primary editLanguage"
                                                    onclick="fetch_language({{ $language->id }})"><span
                                                    class="material-icons"
                                                    style="font-size: 20px">edit</span>
                                            </button>
                                        </div>
                                        <div class="col-md-2 col-xs-5">
                                            <form action="{{ route('lang.destroy', $language) }}" method="POST">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class="btn btn-danger pull-right"><span
                                                        class="material-icons"
                                                        style="font-size: 20px">delete_forever</span>
                                                </button>
                                            </form>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        @endforeach
                    @endisset
                    <div class="col-md-2 hidden" id="languageForm">
                        <div class="card">
                            <div class="header">
                                <div class="pull-right close" id="closeLanguageForm"
                                     style="color: darkred; font-size: 22px"><i
                                        class="pe-7s-close-circle"></i>
                                </div>
                                <h4 class="title">Language Form</h4>
                            </div>
                            <div class="content">
                                <form id="languageFormElement" method="POST">
                                    @csrf
                                    <input id="languageFormMethod" type="hidden" name="_method" value="POST">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="languageName">Language Name</label>
                                                <input class="form-control" required type="text" name="languageName"
                                                       id="languageName">

                                                <br>

                                                <label for="languageIconName">Override Icon Name (optional)</label>
                                                <div class="input-group">
                                                    <span class="input-group-addon" style="background-color: lightgray"
                                                          id="faAddon">fab fa-</span>
                                                    <input class="form-control" type="text" name="languageIconName"
                                                           id="languageIconName">
                                                </div>
                                                <input type="hidden" name="isEdit" value="false">
                                            </div>
                                        </div>
                                    </div>
                                    <input type="submit" class="btn btn-info btn-fill pull-right"
                                           value="Save">
                                    <div class="clearfix"></div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2" id="newLanguageElement">
                        <div class="card">
                            <div class="content text-center">
                                <button class="btn btn-success newLanguage"><span class="material-icons"
                                                                                  style="font-size: 20px">add_circle_outline</span>New
                                    Language
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script type="text/javascript">
        $(document).ready(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        });

        $('#closeLanguageForm').on('click', () => {
            $('#languageForm').addClass('hidden');
            $('#newLanguageElement').removeClass('hidden');

            $('#languageName').val(null);
            $('#languageIconName').val(null);
            $('#isEdit').val(false);

            $('#languageFormElement').attr('action', null);
            $('#languageFormMethod').val('POST');

            $('.editLanguage').attr('disabled', false);
            $('.newLanguage').attr('disabled', false);
        })

        $('.newLanguage').on('click', () => {
            $('#languageForm').removeClass('hidden');
            $('#newLanguageElement').addClass('hidden');

            $('#languageFormElement').attr('action', '/panel/languages/store');
            $('#languageFormMethod').val('POST');

            $('#isEdit').val(false);
            $('.editLanguage').attr('disabled', true);
        })

        function showEditForm(language) {
            $('.newLanguage').attr('disabled', true);
            $('.editLanguage').attr('disabled', true);

            $('#languageName').val(language.name);
            $('#languageIconName').val(language.icon_name);
            $('#isEdit').val(true);
            $('#languageFormElement').attr('action', '/panel/languages/' + language.id);
            $('#languageFormMethod').val('PUT');

            $('#languageForm').removeClass('hidden');
        }

        function fetch_language(languageId) {
            $.ajax({
                url: '/panel/languages/' + languageId,
                type: 'GET',
            })
                .done(function (results) {
                    showEditForm(results[0]);
                })
                .fail(function (XMLHttpRequest, textStatus, errorThrown) {
                    console.error("Error: " + errorThrown);
                    $.notify({
                        icon: 'pe-7s-close-circle',
                        message: "AJAX error " + textStatus + "! Please contact the application administrator."
                    }, {
                        type: 'danger',
                        timer: 4000
                    });
                });
        }
    </script>
@endpush
