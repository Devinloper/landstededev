@extends('admin.layouts.app', ['titlePage' => 'Uitgelichte Video'])


@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md">
                    <div class="card">
                        <div class="header">
                            <h4 class="title">Edit Video</h4>
                        </div>
                        <div class="content">
                            <form method="POST" action="{{ route('video.update') }}">
                                @csrf
                                @method('PUT')
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon" id="basic-addon1"><ion-icon
                                                        name="logo-youtube"></ion-icon></span>
                                                <input type="text" required name="link" class="form-control"
                                                       placeholder="https://www.youtube.com/watch?v=X4eQcnYD4vY"
                                                       value="{{ $video->link ?? '' }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-7">
                                        <div class="form-group">
                                            <div class="col-md-2">
                                                <label for="circle1" class="form-label">
                                                    Circle 1</label>
                                                <input id="circle1" type="color"
                                                       class="form-control form-control-color"
                                                       name="circle1"
                                                       required
                                                       value="{{ $colours->circle1 ?? old('circle1') }}">
                                            </div>
                                            <div class="col-md-2">
                                                <label for="circle2" class="form-label">
                                                    Circle 2</label>
                                                <input id="circle2" type="color"
                                                       class="form-control form-control-color"
                                                       name="circle2"
                                                       required
                                                       value="{{ $colours->circle2 ?? old('circle2') }}">
                                            </div>
                                            <div class="col-md-2">
                                                <label for="circle3" class="form-label">
                                                    Circle 3</label>
                                                <input id="circle3" type="color"
                                                       class="form-control form-control-color"
                                                       name="circle3"
                                                       required
                                                       value="{{ $colours->circle3 ?? old('circle3') }}">
                                            </div>
                                            <div class="col-md-1">
                                                <svg xmlns="http://www.w3.org/2000/svg" height="14" viewBox="0 0 54 14">
                                                    <g fill="none" fill-rule="evenodd" transform="translate(1 1)">
                                                        <circle cx="6" cy="6" r="6"
                                                                fill="{{ $colours->circle1 }}"></circle>
                                                        <circle cx="26" cy="6" r="6"
                                                                fill="{{ $colours->circle2 }}"></circle>
                                                        <circle cx="46" cy="6" r="6"
                                                                fill="{{ $colours->circle3 }}"></circle>
                                                    </g>
                                                </svg>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="form-group">
                                            <label for="border" class="form-label">
                                                Border Colour</label>
                                            <input id="border" type="color"
                                                   class="form-control form-control-color"
                                                   name="border"
                                                   required
                                                   value="{{ $colours->border ?? old('border') }}">
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>

                                <input type="submit" class="btn btn-info btn-fill pull-right" value="Save Changes">
                                <div class="clearfix"></div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
