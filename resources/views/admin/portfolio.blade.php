@extends('admin.layouts.app', ['titlePage' => 'Portfolio'])


@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-8">
                    @isset($projects)
                        @foreach($projects as $project)
                            <div class="col-md-4">
                                <div class="card">
                                    <div class="header text-center">
                                        <div>
                                            <div class="img-fluid"
                                                 style='background: url("{{ asset('storage/projects/' . $project->image_path) }}") no-repeat center center; background-size: cover; padding-top: 56.25%;'></div>
                                        </div>
                                        <br>
                                        <h4 class="title">{{ $project->name }}</h4>
                                    </div>
                                    <div class="content">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="form-group text-center">
                                                    <button onclick="fetch_project({{ $project->id }})"
                                                            class="btn btn-primary editProject col-6"
                                                            type="button">
                                                            <span class="material-icons"
                                                                  style="font-size: 20px">edit</span>Edit Project
                                                    </button>
                                                    <form class="col-6" method="POST"
                                                          action="{{ route('project.destroy', $project) }}">
                                                        @csrf
                                                        @method('DELETE')
                                                        <button type="submit"
                                                                class="btn btn-danger"><span
                                                                class="material-icons" style="font-size: 20px">delete_forever</span>Delete
                                                            Project
                                                        </button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        @endforeach
                    @endisset
                    <div class="col-md-4">
                        <div class="card" id="newProjectCard">
                            <div class="content">
                                <div class="row text-center">
                                    <button id="newProject" class="btn btn-success"
                                            type="button">
                                    <span class="material-icons"
                                          style="font-size: 20px">add_circle_outline</span>New Project
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card hidden" id="projectForm">
                        <div class="header text-center" id="formHeader">
                            <div class="pull-right close" id="closeProjectForm"
                                 style="color: darkred; font-size: 22px"><i
                                    class="pe-7s-close-circle"></i>
                            </div>
                            <h4 class="title text-left">
                                Project Form
                            </h4>
                        </div>
                        <div class="content">
                            <form id="projectFormElement" method="post" enctype="multipart/form-data">
                                @csrf
                                <input id="projectFormMethod" type="hidden" name="_method" value="POST">
                                <div class="form-group">
                                    <input style="width: 100%" type="file" class="form-control-file btn"
                                           name="projectImage" id="projectImage" required>
                                </div>
                                <div class="form-group">
                                    <label for="projectName">Project Name</label>
                                    <input type="text" class="form-control" name="projectName" id="projectName" required
                                           value="{{ old('projectName') }}">
                                    <br>
                                    <label for="projectDescription">Project Description</label>
                                    <textarea class="form-control" name="projectDescription" id="projectDescription"
                                              cols="30" rows="10" required>{{ old('projectDescription') }}</textarea>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-2">
                                        <label style="margin-left: -8px" for="buttonEnabled">Button</label>
                                        <!-- Rounded switch -->
                                        <label class="switch">
                                            <input type="hidden" name="buttonEnabled" value="0">
                                            <input type="checkbox" id="buttonEnabled" name="buttonEnabled" value="1">
                                            <span class="slider round"></span>
                                        </label>
                                    </div>
                                    <div class="col-md-10">
                                        <label for="buttonText">Button Text</label>
                                        <input type="text" class="form-control" name="buttonText" id="buttonText"
                                               value="{{ old('buttonText') }}">
                                        <br>
                                        <label for="buttonLink">Button Link</label>
                                        <input type="text" class="form-control" name="buttonLink" id="buttonLink"
                                               value="{{ old('buttonLink') }}">
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-5">
                                        <label for="projectWidth">Project Image Width</label>
                                        <br>
                                        <select class="select2-box" style="width: 100%" name="projectWidth"
                                                id="projectWidth" required>
                                            @isset($widthSizes)
                                                @foreach($widthSizes as $widthSize)
                                                    <option value="{{ $widthSize->id }}">{{ $widthSize->name }}</option>
                                                @endforeach
                                            @endisset
                                        </select>
                                    </div>
                                    <div class="col-md-5">
                                        <label for="projectHeight">Project Image Height</label>
                                        <br>
                                        <select class="select2-box" style="width: 100%" name="projectHeight"
                                                id="projectHeight" required>
                                            @isset($heightSizes)
                                                @foreach($heightSizes as $heightSize)
                                                    <option
                                                        value="{{ $heightSize->id }}">{{ $heightSize->name }}</option>
                                                @endforeach
                                            @endisset
                                        </select>
                                    </div>
                                    <div class="col-md-2">
                                        <label style="margin-left: -15px" for="projectFeatured">Featured</label>
                                        <!-- Rounded switch -->
                                        <label class="switch">
                                            <input type="hidden" name="projectFeatured" value="0">
                                            <input type="checkbox" id="projectFeatured" name="projectFeatured"
                                                   value="1">
                                            <span class="slider round"></span>
                                        </label>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label for="projectLanguages">Project Languages/Frameworks</label>
                                        <br>
                                        <select class="select2-box" style="width: 100%" name="projectLanguages[]"
                                                id="projectLanguages" multiple="multiple" required>
                                            @isset($languages)
                                                @foreach($languages as $language)
                                                    <option value="{{ $language->id }}">{{ $language->name }}</option>
                                                @endforeach
                                            @endisset
                                        </select>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <input type="hidden" name="isEdit" id="isEdit" value="false">
                                <button type="submit" class="btn btn-info btn-fill pull-right">Save</button>
                            </form>
                            <div class="clearfix"></div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script type="text/javascript">
        $(document).ready(function () {
            $('.select2-box').select2({
                placeholder: 'Select an option'
            });
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        });

        $('#newProject').on('click', () => {
            $('#projectForm').removeClass('hidden');
            $('#newProjectCard').addClass('hidden');

            $('#projectFormElement').attr('action', '/panel/project/store');
            $('#projectFormMethod').val('POST');

            $('#isEdit').val(false);
            $('.editProject').attr('disabled', true);
            $("html, body").animate({scrollTop: $('#articleForm').offset().top}, "slow");
        })

        $('#closeProjectForm').on('click', () => {
            $('#projectForm').addClass('hidden');
            $('#newProjectCard').removeClass('hidden');

            $('#projectName').val(null);
            $('#projectDescription').val(null);
            $('#projectImagePreview').remove();
            $('#buttonText').val(null);
            $('#buttonLink').val(null);
            $('#isEdit').val(false);
            $('#buttonEnabled').attr('checked', false);
            $('#projectFeatured').attr('checked', false);
            $('#projectWidth').val(null).trigger("change");
            $('#projectHeight').val(null).trigger("change");
            $('#projectLanguages').val(null).trigger("change");
            $('#projectImage').attr('required', true);

            $('#projectFormElement').removeAttr('action');
            $('#projectFormMethod').val('POST');

            $('.editProject').attr('disabled', false);
            $('#newProject').attr('disabled', false);
        })

        function showEditForm(project) {
            $('#formHeader').append('<img id="projectImagePreview" class="img-thumbnail" src="" alt="Project Image">');
            $('#projectImagePreview').attr("src", "{{ asset('storage/projects/') }}/" + project.image_path);

            $('#newProject').attr('disabled', true);
            $('.editProject').attr('disabled', true);

            $('#projectName').val(project.name);
            $('#projectDescription').val(project.description);
            $('#buttonText').val(project.button_text);
            $('#buttonLink').val(project.button_link);
            $('#isEdit').val(true);

            if (project.button_toggle) {
                $('#buttonEnabled').attr('checked', true);
            } else {
                $('#buttonEnabled').attr('checked', false);
            }

            if (project.featured) {
                $('#projectFeatured').attr('checked', true);
            } else {
                $('#projectFeatured').attr('checked', false);
            }

            $('#projectWidth').val(project.width_size_id).trigger("change");
            $('#projectHeight').val(project.height_size_id).trigger("change");

            let languages = [];
            $.each(project.languages, (index, value) => {
                languages.push(value.id)
            })
            $('#projectLanguages').val(languages).trigger("change");

            $('#projectFormElement').attr('action', '/panel/project/' + project.id);
            $('#projectFormMethod').val('PUT');

            $('#projectImage').removeAttr('required');

            $('#projectForm').removeClass('hidden');
            $("html, body").animate({scrollTop: $('#projectForm').offset().top}, "slow");
        }

        function fetch_project(projectId) {
            $.ajax({
                url: '/panel/project/' + projectId,
                type: 'GET',
            })
                .done(function (results) {
                    showEditForm(results);
                })
                .fail(function (XMLHttpRequest, textStatus, errorThrown) {
                    console.error("Error: " + errorThrown);
                    $.notify({
                        icon: 'pe-7s-close-circle',
                        message: "AJAX error " + textStatus + "! Please contact the application administrator."
                    }, {
                        type: 'danger',
                        timer: 4000
                    });
                });
        }


    </script>
@endpush
