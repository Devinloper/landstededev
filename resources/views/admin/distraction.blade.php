@extends('admin.layouts.app', ['titlePage' => 'Meld Je Aan'])


@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md">
                    <div class="card">
                        <div class="header">
                            <h4 class="title">Edit Meld Je Aan</h4>
                        </div>
                        <div class="content">
                            <form method="POST" action="{{ route('distraction.update') }}">
                                @csrf
                                @method('PUT')
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="link">Link for distraction component</label>
                                            <div class="input-group">
                                                <span class="input-group-addon" id="basic-addon1"><ion-icon name="link-outline"></ion-icon></span>
                                                <input type="text" id="link" required name="link" class="form-control" placeholder="https://www.landstedembo.nl/aanmelden/" value="{{ $distraction->link ?? old('link') }}">
                                            </div>
                                            <br>
                                            <label for="text">Text above link</label>
                                            <textarea name="text" id="text" cols="30" rows="10" class="form-control">{!! nl2br(e($distraction->text ?? old('text'))) !!}</textarea>
                                        </div>
                                    </div>
                                </div>

                                <input type="submit" class="btn btn-info btn-fill pull-right" value="Save Changes">
                                <div class="clearfix"></div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
