{{-- The extend passes down the background colour to be used on the <body> tag. --}}
@extends('layouts.app', ['bgColor' => $colours[0]->background])

@section('content')
    <div class="site-wrapper">
        <div class="container-fluid"
             style="background-color: {{ $colours[0]->background }}; color: {{ $colours[0]->text }}; height: 100%">
            <div class="container" style="padding-top: 20px">
                <div class="row">
                    <div class="col-12 text-center">
                        <h1 class="title">News</h1>
                    </div>
                </div>
                @foreach($articles as $article)
                    <div class="row" style="padding-bottom: 30px;">
                        <div class="col-12">
                            <a href="{{ route('news.article', $article) }}" class="text-decoration-none"
                               style="color: inherit">
                                <div class="card" style="background-color: {{ $colours[0]->background }};">
                                    <div class="header text-center">
                                        <div style="margin-top: 20px;" class="col-11 mx-auto">
                                            <div class="img-fluid img-thumbnail"
                                                 style='background: url("{{ asset('storage/articles/' . $article->image_path) }}") no-repeat center center; background-size: cover; padding-top: 56.25%;'></div>
                                        </div>
{{--                                        <img src="{{ asset('storage/articles/' . $article->image_path)  }}" alt=""--}}
{{--                                             class="img-thumbnail img-fluid" style="margin-top: 20px;">--}}
                                        <h1 class="title">{{ $article->name }}</h1>
                                    </div>
                                    <div class="content">
                                        <div class="col-11 mx-auto">
                                            <p>{!! nl2br(e(substr($article->content, 0, 200) ?? '') . '...') !!} <a
                                                    href="{{ route('news.article', $article) }}">Lees meer...</a></p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>

    @include('footer', ['colours' => $colours[1], $footer])
@endsection

