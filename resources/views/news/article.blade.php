{{-- The extend passes down the background colour to be used on the <body> tag. --}}
@extends('layouts.app', ['bgColor' => $colours[0]->background])

@section('content')
    <div class="site-wrapper">
        <div class="container-fluid"
             style="background-color: {{ $colours[0]->background }}; color: {{ $colours[0]->text }}; height: 100%;">
            <div class="container" style="padding-top: 20px">
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-12 text-center">
                            <div class="col-md-11 mx-auto">
                                <div class="img-fluid img-thumbnail"
                                     style='background: url("{{ asset('storage/articles/' . $article->image_path) }}") no-repeat center center; background-size: cover; padding-top: 56.25%;'></div>
                            </div>
{{--                            <img src="{{ asset('storage/articles/' . $article->image_path) }}" alt=""--}}
{{--                                 class="img-thumbnail img-responsive" style="margin:0 auto;">--}}
                        </div>
                        <br>
                        <h1 class="title text-center">{{ $article->name }}</h1>
                        <div class="col-md-11 mx-auto">
                            <p style="word-wrap: break-word"
                               class="content">{!! nl2br(e($article->content ?? '')) !!}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('footer', ['colours' => $colours[1], $footer])
@endsection
