@extends('layouts.app')

@section('content')
    <div class="site-wrapper">

        <!-- Portfolio Pop-Up -->
        <div style="display: none; min-height: 100%;" class="container-fluid" id="selectableModal">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center" style="max-height: 50%; padding-bottom: 20px">
                        <img class="img-fluid" id="modalImage" src="#" alt="#">
                    </div>
                </div>
                <div class="row" style="padding-bottom: 20px;">
                    <div class="col-md-12">
                        <h2 class="text-center" id="modalTitle" data-selectable="true"></h2>
                        <p id="modalDescription" data-selectable="true"></p>
                    </div>
                </div>
                <div class="row" id="modalLanguages" style="padding-bottom: 20px;" data-selectable="true"></div>
                <div class="row">
                    <div class="col-md-12 text-center">
                        <a id="modalButton" href="#">
                            <button type="button" class="btn btn-dark btn-lg" data-selectable="true" href="#">Visit
                                Project
                            </button>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Portfolio Pop-Up -->

        <!-- Video Section -->
        <section>
            <div class="container-fluid"
                 style="background-color: {{ $colours[0]->background }}; color: {{ $colours[0]->text }}">
                <div class="container">
                    <div class="row align-items-center justify-content-center">
                        <div class="col text-center" style="margin: 100px 0;">
                            <div style="border: 2px solid {{ $videoColours->border }}; padding: 10px 0 45px;">
                                <div class="col-1 mx-4" style="padding-bottom: 5px;">
                                    <svg xmlns="http://www.w3.org/2000/svg" height="14" viewBox="0 0 54 14">
                                        <g fill="none" fill-rule="evenodd" transform="translate(1 1)">
                                            <circle cx="6" cy="6" r="6" fill="{{ $videoColours->circle1 }}"></circle>
                                            <circle cx="26" cy="6" r="6" fill="{{ $videoColours->circle2 }}"></circle>
                                            <circle cx="46" cy="6" r="6" fill="{{ $videoColours->circle3 }}"></circle>
                                        </g>
                                    </svg>
                                </div>
                                <div class="col-11 mx-auto">
                                    <div class="ratio ratio-16x9">
                                        <iframe
                                            src="{{ $video->link ?? 'https://www.youtube.com/embed/GfZrGE4yWwI' }}"
                                            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                            allowfullscreen title="Uitgelichte Video"></iframe>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- End Video Section -->

        <!-- Include distraction component -->
    @include('distraction', [$colours, $distraction])

    <!-- News Section -->
        <section>
            <div class="container-fluid text-center"
                 style="background-color: {{ $colours[3]->background }}; color: {{ $colours[3]->text }}; padding: 20px 0;">
                <div class="container">
                    <div class="col">
                        <h1>News</h1>
                        <div class="owl-carousel owl-theme">
                            @foreach($news as $article)
                                <a href="news/article/{{ $article->id }}">
                                    <div>
                                        <div class="img-fluid"
                                             style='background: url("{{ asset('storage/articles/' . $article->image_path) }}") no-repeat center center; background-size: cover; padding-top: 56.25%;'></div>
                                    </div>
                                </a>
                            @endforeach
                        </div>
                    </div>
                    <div class="col">
                        <a href="{{ route('news.index') }}" class="btn btn-sm"
                           style="background-color: {{ $colours[4]->background }}; color: {{ $colours[4]->text }}; margin-top: 20px;">Lees
                            alles</a>
                    </div>
                </div>
            </div>
        </section>
        <!-- End News Section -->

        <!-- Portfolio Section -->
        <section id="portfolioSection">
            <div class="container-fluid text-center"
                 style="background-color: {{ $colours[5]->background }}; color: {{ $colours[5]->text }}; padding: 20px 0">
                <div class="container" style="padding-bottom: 20px">
                    <div class="row">
                        <h1>Portfolio</h1>
                        <div class="col-md-12">
                            <div id="grid" class="grid" style="height: 1200px">
                                <div class="grid-sizer" style="width: 16.667%"></div>
                                @isset($projects)
                                    @foreach($projects as $project)
                                        <a onclick="fetch_project({{ $project->id }})" style="cursor: pointer;">
                                            <div
                                                class="grid-item grid-item--{{ $project->widthSize->class_name }}  grid-item--{{ $project->heightSize->class_name }} ">
                                                <img class="img-responsive"
                                                     src="{{ asset('storage/projects/' . $project->image_path) }}"
                                                     alt="{{ $project->name }}">
                                            </div>
                                        </a>
                                    @endforeach
                                @endisset
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- End Portfolio Section -->

        <!-- Include distraction component -->
    @include('distraction', [$colours, $distraction])

    <!-- Include Footer component -->
        @include('footer', ['colours' => $colours[6], $footer])
    </div>
@endsection

@push('js')
    <script type="text/javascript" src="{{ asset('js/owl.carousel.min.js') }}"></script>
    <script async type="text/javascript" src="https://unpkg.com/packery@2.1.2/dist/packery.pkgd.min.js"></script>
    <script async type="text/javascript" src="{{ asset('js/jquery.lockscroll.min.js') }}"></script>
    <script src="https://unpkg.com/imagesloaded@4/imagesloaded.pkgd.min.js"></script>
    <script async src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
@endpush

@push('js')
    <script type="text/javascript">
        /* Define variables that are used multiple times or for convenience */
        let $grid;
        let modalImage = $('#modalImage');
        let modalButton = $('#modalButton');
        let carousel = $('.owl-carousel');

        $(document).ready(function () {
            /* Initialize the news carousel */
            carousel.owlCarousel({
                margin: 10,
                center: true,
                autoplay: true,
                dots: true,
                dotsEach: true,
                nav: false,
                loop: true
            });

            /* Initialize the portfolio grid after all images have loaded in */
            $grid = $('.grid').imagesLoaded(function () {
                $grid = $('.grid').packery({
                    itemSelector: '.grid-item',
                    percentPosition: true
                });
            });
        });

        /* Function that gets called whenever the user clicks on a project in the portfolio grid.
        * The function will get the project from the database and configure all the data in the pop-up, then show the pop-up */
        function fetch_project(projectId) {
            $.ajax({
                url: "/panel/project/" + projectId,
                type: 'GET'
            })
                .done(function (results) {
                    modalImage.attr('src', "{{ asset('/storage/projects/') }}/" + results.image_path);
                    modalImage.attr('alt', results.image_path);

                    $('#modalTitle').text(results.name);
                    $('#modalDescription').text(results.description);
                    if (parseInt(results.button_toggle)) {
                        modalButton.attr('href', results.button_link);
                        modalButton.children().text(results.button_text);
                        modalButton.show();
                    } else {
                        modalButton.attr('href', '#');
                        modalButton.children().text('#');
                        modalButton.hide();
                    }

                    /* Loop through all languages linked to the project and append the one-by-one */
                    $.each(results.languages, (index, value) => {
                        let name = value.name;
                        /* If the icon name is not overridden by another icon in the admin panel, use the name of the language in lowercase.
                        * Otherwise, if the icon name has been overridden, use that instead. */
                        let className = (value.icon_name != null && value.icon_name !== '') ? value.icon_name : name.toLowerCase();
                        $('#modalLanguages').append('<div class="col-md-3" style="font-size: 30px"><i class="fab fa-' + className + '" style="font-size: 50px"></i> ' + name + '</div>')
                    })

                    /* Open the pop-up for the project that was clicked on. */
                    $.fancybox.open({
                        src: '#selectableModal',
                        type: 'inline',
                        opts: {
                            afterShow: function (instance, current) {
                                /* Once the pop-up is open, lock the scrolling of the page behind the pop-up. */
                                $.lockScroll(true);
                            },
                            afterClose: function (instance, current) {
                                /* Once the pop-up is closed, unlock the scrolling of the page behind the pop-up
                                * and remove all the appended language/framework elements. */
                                $.lockScroll(false);
                                $('#modalLanguages').children().remove();
                            },
                            touch: false,
                            smallBtn: true,
                            /* Override the default button to be bigger. */
                            btnTpl: {
                                smallBtn: '<button style="height: 80px; width: 80px;" data-fancybox-close class="fancybox-button fancybox-close-small" title="Close"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path style="width: 100%; height: 100%;" d="M13 12l5-5-1-1-5 5-5-5-1 1 5 5-5 5 1 1 5-5 5 5 1-1z"/></svg></button>'
                            },
                        }
                    });
                });
        }

    </script>
@endpush
