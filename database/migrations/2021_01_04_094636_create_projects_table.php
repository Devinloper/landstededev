<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->id();
            $table->char('name');
            $table->text('description');
            $table->char('image_path');
            $table->boolean('button_toggle');
            $table->char('button_text')->nullable();
            $table->char('button_link')->nullable();
            $table->boolean('featured');
            $table->bigInteger('height_size_id')->unsigned();
            $table->bigInteger('width_size_id')->unsigned();
            $table->timestamps();

            $table->foreign("height_size_id")->references("id")->on("height_sizes");
            $table->foreign("width_size_id")->references("id")->on("width_sizes");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
