<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserSeeder::class);
        $this->call(ColourSeeder::class);
        $this->call(DistractionSeeder::class);
        $this->call(WidthSizeSeeder::class);
        $this->call(HeightSizeSeeder::class);
        $this->call(FooterSeeder::class);
        $this->call(VideoSeeder::class);
    }
}
