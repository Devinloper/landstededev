<?php

use Illuminate\Database\Seeder;

class DistractionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('distractions')->insert([
            'id' => 1,
            'link' => 'https://www.landstedembo.nl/aanmelden/',
            'text' => 'Software ontwikkelen: lijkt jou dat wel wat? Tijdens deze opleiding word je helemaal klaargestoomd tot software developer. Als software-ontwikkelaar in de ICT maak je software voor verschillende apparaten, zoals computers, telefoons of tablets. Aan wat voor software moet je dan denken? Nou, bijvoorbeeld de voorkant of achterkant van websites, apps of programma’s. Maar denk ook aan het programmeren van games, apps, applicaties en software voor medische of andere specialistische bedrijven.'
        ]);
    }
}
