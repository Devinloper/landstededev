<?php

use Illuminate\Database\Seeder;

class FooterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('footers')->insert([
            'id' => 1,
            'title' => 'Title Pog',
            'content' => 'Software ontwikkelen: lijkt jou dat wel wat? Tijdens deze opleiding word je helemaal klaargestoomd tot software developer.'
        ]);
    }
}
