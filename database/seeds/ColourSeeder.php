<?php

use Illuminate\Database\Seeder;

class ColourSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('colours')->insert([
            'id' => 1,
            'name' => 'Uitgelichte Video',
            'text' => '#000000',
            'background' => '#f0f0f0'
        ]);
        DB::table('colours')->insert([
            'id' => 2,
            'name' => 'Meld Je Aan',
            'text' => '#000000',
            'background' => '#f0f0f0'
        ]);
        DB::table('colours')->insert([
            'id' => 3,
            'name' => 'Meld Je Aan Button',
            'text' => '#000000',
            'background' => '#f0f0f0'
        ]);
        DB::table('colours')->insert([
            'id' => 4,
            'name' => 'Nieuws',
            'text' => '#000000',
            'background' => '#f0f0f0'
        ]);
        DB::table('colours')->insert([
            'id' => 5,
            'name' => 'Nieuws Button',
            'text' => '#000000',
            'background' => '#f0f0f0'
        ]);
        DB::table('colours')->insert([
            'id' => 6,
            'name' => 'Portfolio',
            'text' => '#000000',
            'background' => '#f0f0f0'
        ]);
        DB::table('colours')->insert([
            'id' => 7,
            'name' => 'Footer',
            'text' => '#000000',
            'background' => '#f0f0f0'
        ]);
    }
}
