<?php

use Illuminate\Database\Seeder;

class HeightSizeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('height_sizes')->insert([
            'id' => 1,
            'name' => 'Small',
            'class_name' => 'small-height'
        ]);
        DB::table('height_sizes')->insert([
            'id' => 2,
            'name' => 'Double Small',
            'class_name' => 'double-small-height'
        ]);
        DB::table('height_sizes')->insert([
            'id' => 3,
            'name' => 'Half',
            'class_name' => 'half-height'
        ]);
        DB::table('height_sizes')->insert([
            'id' => 4,
            'name' => 'Three Fourth',
            'class_name' => 'three-fourth-height'
        ]);
        DB::table('height_sizes')->insert([
            'id' => 5,
            'name' => 'Big',
            'class_name' => 'big-height'
        ]);
        DB::table('height_sizes')->insert([
            'id' => 6,
            'name' => 'Full',
            'class_name' => 'full-height'
        ]);
    }
}
