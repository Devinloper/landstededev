<?php

use Illuminate\Database\Seeder;

class VideoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('videos')->insert([
            'id' => 1,
            'link' => 'https://www.youtube.com/embed/GfZrGE4yWwI',
            'colours' => '{"circle1":"#FF5F56","circle2":"#FFBD2E","circle3":"#27C93F","border":"#000000"}'
        ]);
    }
}
