<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');

Auth::routes(['register' => false]);

Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout')->name('logout');

Route::get('/panel', 'AdminController@index')->name('admin.dashboard')->middleware('auth');

Route::get('/panel/video', 'VideoController@index')->name('admin.video')->middleware('auth');
Route::put('/panel/video', 'VideoController@update')->name('video.update')->middleware('auth');

Route::get('/panel/user', 'UserController@index')->name('admin.user')->middleware('auth');
Route::put('/panel/user', 'UserController@update')->name('user.update')->middleware('auth');

Route::get('/panel/colours', 'ColourController@index')->name('admin.colours')->middleware('auth');
Route::put('/panel/colours/{colour}', 'ColourController@update')->name('colour.update')->middleware('auth');

Route::get('/panel/distraction', 'DistractionController@index')->name('admin.distraction')->middleware('auth');
Route::put('/panel/distraction', 'DistractionController@update')->name('distraction.update')->middleware('auth');

Route::get('/news', 'ArticleController@index')->name('news.index');
Route::get('/news/article/{article}', 'ArticleController@show')->name('news.article');
/* If the user navigates to '/news/article/' without an article id, redirect them back to the news index page. */
Route::get('/news/article', function () { return redirect()->route('news.index'); });

Route::get('/panel/news', 'ArticleController@create')->name('admin.news')->middleware('auth');
Route::post('/panel/news/store', 'ArticleController@store')->name('news.store')->middleware('auth');
Route::put('/panel/news/{article}', 'ArticleController@update')->name('news.update')->middleware('auth');
Route::get('/panel/news/{article}', 'ArticleController@edit')->name('news.edit')->middleware('auth');
Route::delete('/panel/news/{article}', 'ArticleController@destroy')->name('article.destroy')->middleware('auth');

Route::get('/panel/languages', 'LanguageController@index')->name('admin.languages')->middleware('auth');
Route::post('/panel/languages/store', 'LanguageController@store')->name('lang.store')->middleware('auth');
Route::put('/panel/languages/{language}', 'LanguageController@update')->name('lang.update')->middleware('auth');
Route::get('/panel/languages/{language}', 'LanguageController@edit')->name('lang.edit')->middleware('auth');
Route::delete('/panel/languages/{language}', 'LanguageController@destroy')->name('lang.destroy')->middleware('auth');

Route::get('/panel/footer', 'FooterController@index')->name('admin.footer')->middleware('auth');
Route::put('/panel/footer/{footer}', 'FooterController@update')->name('footer.update')->middleware('auth');

Route::get('/panel/links', 'LinkController@index')->name('link.index')->middleware('auth');
Route::post('/panel/links', 'LinkController@store')->name('link.store')->middleware('auth');
Route::put('/panel/links/{link}', 'LinkController@update')->name('link.update')->middleware('auth');
Route::delete('/panel/links/{link}', 'LinkController@destroy')->name('link.destroy')->middleware('auth');

Route::get('/panel/portfolio', 'ProjectController@index')->name('admin.portfolio')->middleware('auth');
Route::get('/panel/project/{project}', 'ProjectController@show')->name('project.show');
Route::post('/panel/project/store', 'ProjectController@store')->name('project.store')->middleware('auth');
Route::put('/panel/project/{project}', 'ProjectController@update')->name('project.update')->middleware('auth');
Route::delete('/panel/project/{project}', 'ProjectController@destroy')->name('project.destroy')->middleware('auth');
