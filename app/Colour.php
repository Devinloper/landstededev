<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Colour extends Model
{
    /* Provide the model with the values that are able to be filled in using other logic. */
    protected $fillable = ['name', 'background', 'text'];
}
