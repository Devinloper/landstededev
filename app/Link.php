<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Link extends Model
{
    /* Provide the model with the values that are able to be filled in using other logic. */
    protected $fillable = ['label', 'link', 'colour_label', 'colour_button', 'footer_id'];

    /* Set up the relation with the Footer model. */
    public function footer()
    {
        return $this->belongsTo('App\Footer');
    }
}
