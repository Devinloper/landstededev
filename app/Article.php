<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    /* Provide the model with the values that are able to be filled in using other logic. */
    protected $fillable = ['name', 'content', 'image_path'];
}
