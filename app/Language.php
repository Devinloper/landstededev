<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
    /* Provide the model with the values that are able to be filled in using other logic. */
    protected $fillable = ['name', 'icon_name'];

    /* Set up the relation with the Project model. */
    public function projects()
    {
        return $this->belongsToMany('App\Project');
    }

}
