<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    /* Provide the model with the values that are able to be filled in using other logic. */
    protected $fillable = ['link', 'colours'];
}
