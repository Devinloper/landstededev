<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    /* Provide the model with the values that are able to be filled in using other logic. */
    protected $fillable = ['name', 'description', 'image_path', 'button_toggle', 'button_text', 'button_link', 'featured', 'width_size_id', 'height_size_id'];

    /* Set up the relation with the Language model. */
    public function languages()
    {
        // any project van have many languages
        // any language may be applied to many projects
        return $this->belongsToMany('App\Language');
    }

    /* Set up the relation with the HeightSize model. */
    public function heightSize()
    {
        return $this->belongsTo('App\HeightSize');
    }

    /* Set up the raltion with the WidthSize model. */
    public function widthSize()
    {
        return $this->belongsTo('App\WidthSize');
    }
}
