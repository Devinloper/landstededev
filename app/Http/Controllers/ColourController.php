<?php

namespace App\Http\Controllers;

use App\Colour;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ColourController extends Controller
{
    /**
     * Display a listing of all the colours to be edited and viewed on the admin panel.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function index()
    {
        $colours = Colour::all();
        return view('admin.colours', compact(['colours']));
    }

    /**
     * Update the specified colour in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $colour
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Colour $colour)
    {
        $this->validateInput();

        $colour->update(['text' => $request->textColour, 'background' => $request->backgroundColour]);

        return redirect()->back()->with('message', 'Colour updated successfully');
    }

    /*
     * Validate the text input.
     * */
    private function validateInput()
    {
        /* The colours must all be a 6 character HEX codes (#000000).
        Because the colour picker on the webpage does not recognize other colour code formats. */
        $rules = [
            'textColour' => ['required', 'regex:/^(\#[\da-f]{6})$/'],
            'backgroundColour' => ['required', 'regex:/^(\#[\da-f]{6})$/']
        ];

        Validator::make(request()->all(), $rules)->validate();
    }
}
