<?php

namespace App\Http\Controllers;

use App\Video;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class VideoController extends Controller
{
    /**
     * Display the admin panel page for the video.
     *
     * @return \Illuminate\Http\Response|\Illuminate\View\View
     */
    public function index()
    {
        $video = Video::first();
        $colours = json_decode($video->colours);
        return view('admin.video', compact(['video', 'colours']));
    }

    /**
     * Update the video data in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request)
    {
        $youtubeEmbedLink = 'https://www.youtube.com/embed/';

        $this->validateInput();
        $video = Video::first();
        $colours = json_encode(['circle1' => $request->circle1, 'circle2' => $request->circle2, 'circle3' => $request->circle3, 'border' => $request->border]);

        /* If the link contains a 'v=' substring, it's a normal YouTube link.
        In the case that the link is a general YouTube link, we attach the video ID to the predefined embed link.
        In case that it is not a general YouTube link, we assume that the link is already an embed link. */
        if (isset(explode('v=', $request->link)[1])) {
            $youtubeEmbedLink .= explode('v=', $request->link)[1];
        } else {
            $youtubeEmbedLink = $request->link;
        }

        $video->update([
            'link' => $youtubeEmbedLink,
            'colours' => $colours
        ]);

        return redirect()->back()->with('message', 'Video updated successfully');
    }

    /*
     * Validate the text input.
     * */
    private function validateInput()
    {
        /* The circles and border must all be a 6 character HEX code (#000000).
        Because the colour picker on the webpage does not recognize other colour code formats. */
        $rules = [
            'link' => ['required', 'active_url'],
            'circle1' => ['required', 'regex:/^(\#[\da-f]{6})$/'],
            'circle2' => ['required', 'regex:/^(\#[\da-f]{6})$/'],
            'circle3' => ['required', 'regex:/^(\#[\da-f]{6})$/'],
            'border' => ['required', 'regex:/^(\#[\da-f]{6})$/']
        ];

        Validator::make(request()->all(), $rules)->validate();
    }

}
