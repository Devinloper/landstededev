<?php

namespace App\Http\Controllers;

use App\Language;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class LanguageController extends Controller
{
    /**
     * Gets all Language data from the database to be viewed and edited in the admin panel.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function index()
    {
        $languages = Language::all();
        return view('admin.languages', compact(['languages']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $this->validateInput();

        Language::create([
            'name' => $request->languageName,
            'icon_name' => $request->languageIconName
        ]);

        return redirect()->back()->with('message', 'New Language Created Successfully');
    }

    /**
     * This method is called by an AJAX request on the Language admin panel page whenever an Edit button gets clicked.
     * It will return the desired resource.
     *
     * @param  \App\Language  $language
     * @return \Illuminate\Http\Response
     */
    public function edit(Language $language)
    {
        return Language::find($language);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Language  $language
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Language $language)
    {
        $this->validateInput();

        $language->update([
            'name' => $request->languageName,
            'icon_name' => $request->languageIconName
        ]);

        return redirect()->back()->with('message', 'Language updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Language  $language
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Language $language)
    {
        try {
            $language->delete();
        } catch (\Exception $e) {
            return redirect()->back()->withErrors('Something went wrong! ' . $e);
        }

        return redirect()->back()->with('message', 'Language deleted successfully');
    }

    /* Validate the text input. */
    private function validateInput()
    {
        $rules = [
            'isEdit' => ['required'],
            'languageName' => ['required', 'max:255'],
            'languageIconName' => ['max:255']
        ];

        $v = Validator::make(request()->all(), $rules);
        $v->validate();
    }
}
