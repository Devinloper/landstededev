<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdminController extends Controller
{
    /* Display the landing page for the admin dashboard. This page has no other functionality. */
    public function index()
    {
        return view('admin.dashboard');
    }
}
