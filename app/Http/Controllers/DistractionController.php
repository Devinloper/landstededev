<?php

namespace App\Http\Controllers;

use App\Distraction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class DistractionController extends Controller
{
    /**
     * Gets the distraction page component data from the database to be edited and viewed in the admin panel.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $distraction = Distraction::first();

        return view('admin.distraction', compact(['distraction']));
    }

    /**
     * Update the specified resource in storage.
     * We don't pass the Distraction id to the function, because there is always only one in the database.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request)
    {
        $this->validateInput();

        $distraction = Distraction::first();

        $distraction->update(['link' => $request->link, 'text' => $request->text]);

        return redirect()->back()->with('message', 'Distraction updated successfully');
    }

    /*
     * Validate the text input.
     * */
    private function validateInput()
    {
        $rules = [
            'link' => ['required', 'active_url'],
            'text' => ['required']
        ];

        Validator::make(request()->all(), $rules)->validate();
    }
}
