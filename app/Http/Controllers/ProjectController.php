<?php

namespace App\Http\Controllers;

use App\HeightSize;
use App\Language;
use App\Project;
use App\WidthSize;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;

class ProjectController extends Controller
{
    /**
     * Get all projects, sizes and languages to view and edit in the admin panel.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function index()
    {
        $projects = Project::all();
        $widthSizes = WidthSize::all();
        $heightSizes = HeightSize::all();
        $languages = Language::all();
        return view('admin.portfolio', compact(['projects', 'languages', 'widthSizes', 'heightSizes']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $this->validateInput();
        $image = $this->validateImage();

        $project = Project::create([
            'name' => $request->projectName,
            'description' => $request->projectDescription,
            'image_path' => $image,
            'button_toggle' => $request->buttonEnabled,
            'button_text' => $request->buttonText,
            'button_link' => $request->buttonLink,
            'featured' => $request->projectFeatured,
            'width_size_id' => $request->projectWidth,
            'height_size_id' => $request->projectHeight
        ]);

        /* Using Eloquents' attach method we can make entries into the pivot table without having to do it manually.
        This loops through all languages chosen in the form and creates a database entry for each one. */
        foreach ($request->projectLanguages as $language)
            $project->languages()->attach($language);

        return redirect()->back()->with('message', 'New Project Created Successfully');
    }

    /**
     * Gets the desired project to be either edited in the admin panel or shown on the home page.
     *
     * @param Project $project
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|\Illuminate\Http\Response|object
     */
    public function show(Project $project)
    {
        return Project::with('languages')->where('id', $project->id)->first();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param Project $project
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Project $project)
    {
        $this->validateInput();

        /* Set the image path default to the image path already in storage.
        If the request contains a new image, validate and use that image instead. */
        $image = $project->image_path;
        if (isset($request->projectImage))
            $image = $this->validateImage($project);

        $project->update([
            'name' => $request->projectName,
            'description' => $request->projectDescription,
            'image_path' => $image,
            'button_toggle' => $request->buttonEnabled,
            'button_text' => $request->buttonText,
            'button_link' => $request->buttonLink,
            'featured' => $request->projectFeatured,
            'width_size_id' => $request->projectWidth,
            'height_size_id' => $request->projectHeight
        ]);

        /* PHP did not like using the array that gets passed down from the form directly, so I add each language from
        the form to a new array that PHP can manage without throwing errors. */
        $languages = [];
        foreach ($request->projectLanguages as $projectLanguage) {
            array_push($languages, $projectLanguage);
        }

        /* Using Eloquents' sync method, we can automatically delete links in the pivot table that aren't in the
        language array and add the ones that are new. Without having to do any manual loops. */
        $project->languages()->sync($languages);

        return redirect()->back()->with('message', 'Project updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Project $project
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Project $project)
    {
        try {
            if (file_exists('storage/projects/' . $project->image_path)) {
                File::delete('storage/projects/' . $project->image_path);
            }
            $project->delete();
        } catch (\Exception $e) {
            return redirect()->back()->withErrors('Something went wrong! ' . $e);
        }

        return redirect()->back()->with('message', 'Project deleted successfully');
    }

    /* Validate the text input. */
    private function validateInput()
    {
        /* Unless the Enable Button switch in the form is enabled, the buttonText and buttonLink will not be included
        in the validation. */
        $rules = [
            'isEdit' => ['required'],
            'projectName' => ['required', 'max:255'],
            'projectDescription' => ['required'],
            'buttonEnabled' => ['required'],
            'buttonText' => ['exclude_unless:buttonEnabled,1', 'max:255', 'required'],
            'buttonLink' => ['exclude_unless:buttonEnabled,1', 'max:255', 'active_url', 'required'],
            'projectFeatured' => ['required'],
            'projectWidth' => ['required', 'integer'],
            'projectHeight' => ['required', 'integer']
        ];

        $v = Validator::make(request()->all(), $rules);
        $v->validate();
    }

    /* Validate the image input. */
    private function validateImage(Project $project = null)
    {
        $rules = [
            'isEdit' => ['required'],
            'projectImage' => ['mimes:jpeg,jpg,png,bmp']
        ];

        $v = Validator::make(request()->all(), $rules);

        /* If the isEdit value from the request is false, then the projectImage will be required. */
        $v->sometimes(['projectImage'], ['required', 'mimes:jpeg,jpg,png,bmp'], function ($input) {
            return $input->isEdit = 'false';
        });

        $v->validate();

        $image = request()->file('projectImage');

        /* Using the Intervention Image package we can make the file easy to manipulate and save. */
        $file = Image::make($image);

        $destinationPath = 'storage/projects/';

        if (request()->isEdit == 'true') {
            $imageName = 'projectImage_' . $project->id;
        } else {
            /* Get the next auto-incremented id that will be used in the database, and use it for the image name. */
            $nextId = DB::select("SHOW TABLE STATUS LIKE 'projects'")[0]->Auto_increment;
            $imageName = 'projectImage_' . $nextId;
        }

        $extension = request()->file('projectImage')->getClientOriginalExtension();

        // If the projects directory does not exist, create it.
        if (!Storage::disk('public')->exists('projects/'))
            Storage::disk('public')->makeDirectory('projects/');

        $fileName = $imageName . '.' . $extension;

        if (Storage::disk('public')->exists($destinationPath . '/' . $imageName)) {
            Storage::disk('public')->delete($destinationPath . '/' . $imageName);
        }

        /* Save the image to public storage. */
        $file->save(public_path($destinationPath . $fileName));

        return $fileName;
    }
}
