<?php

namespace App\Http\Controllers;

use App\Article;
use App\Colour;
use App\Distraction;
use App\Footer;
use App\Project;
use App\Video;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Show the home page of the application.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $video = Video::first();

        /* The colours for the elements surrounding the video are kept in the same database table.
        So we decode the JSON string to an object from the colours stored there. */
        $videoColours = json_decode($video->colours);

        $colours = Colour::all();
        $distraction = Distraction::first();
        $news = Article::all();
        $projects = Project::all()->where('featured', '=', 1);
        $footer = Footer::first();
        return view('index', compact(['video', 'videoColours', 'colours', 'distraction', 'news', 'projects', 'footer']));
    }
}
