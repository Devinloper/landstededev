<?php

namespace App\Http\Controllers;

use App\Article;
use App\Colour;
use App\Footer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use Symfony\Component\Console\Input\Input;

class ArticleController extends Controller
{
    /**
     * Display a listing of tall articles. This is used for the /news page.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $colours = Colour::find([4, 7]);
        $articles = Article::all();
        $footer = Footer::first();
        return view('news.index', compact(['articles', 'colours', 'footer']));
    }

    /**
     * Gets all articles and passes them to be displayed and edited on the admin news page.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function create()
    {
        $articles = Article::all();
        return view('admin.news', compact(['articles']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $this->validateInput();
        $image = $this->validateImage();

        Article::create([
           'name' => $request->articleName,
           'content' => $request->articleContent,
           'image_path' => $image
        ]);

        return redirect()->back()->with('message', 'New Article Created Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Article $article
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function show(Article $article)
    {
        $colours = Colour::find([4, 7]);
        $footer = Footer::first();

        return view('news.article', compact(['article', 'colours', 'footer']));
    }

    /**
     * Gets called by an Ajax call in the admin panel. Returns the desired article to be edited.
     *
     * @param \App\Article $article
     * @return \Illuminate\Http\Response
     */
    public function edit(Article $article)
    {
        return Article::find($article);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Article $article
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Article $article)
    {
        $this->validateInput();

        /* Set the image path default to the image path already in storage.
        If the request contains a new image, validate and use that image instead. */
        $image = $article->image_path;
        if (isset($request->articleImage))
            $image = $this->validateImage($article);

        $article->update([
            'name' => $request->articleName,
            'content' => $request->articleContent,
            'image_path' => $image
        ]);

        return redirect()->back()->with('message', 'Article updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Article $article
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Article $article)
    {
        try {
            if (file_exists('storage/articles/' . $article->image_path)) {
                File::delete('storage/articles/' . $article->image_path);
            }
            $article->delete();
        } catch (\Exception $e) {
            return redirect()->back()->withErrors('Something went wrong! ' . $e);
        }

        return redirect()->back()->with('message', 'Article deleted successfully');
    }

    /* Validate the text input. */
    private function validateInput()
    {
        $rules = [
            'isEdit' => ['required'],
            'articleName' => ['required', 'max:255'],
            'articleContent' => ['required']
        ];

        $v = Validator::make(request()->all(), $rules);
        $v->validate();
    }

    /* Validate the image */
    private function validateImage(Article $article = null)
    {
        $rules = [
            'isEdit' => ['required'],
            'articleImage' => ['mimes:jpeg,jpg,png,bmp']
        ];

        $v = Validator::make(request()->all(), $rules);

        /* If the isEdit value from the request is false, then the articleImage will be required. */
        $v->sometimes(['articleImage'], ['required', 'mimes:jpeg,jpg,png,bmp'], function ($input) {
            return $input->isEdit = 'false';
        });

        $v->validate();

        $image = request()->file('articleImage');

        /* Using the Intervention Image package we can make the file easy to manipulate and save. */
        $file = Image::make($image);

        $destinationPath = 'storage/articles/';

        if (request()->isEdit == 'true') {
            $imageName = 'articleImage_' . $article->id;
        } else {
            /* Get the next auto-incremented id that will be used in the database, and use it for the image name. */
            $nextId = DB::select("SHOW TABLE STATUS LIKE 'articles'")[0]->Auto_increment;
            $imageName = 'articleImage_' . $nextId;
        }

        $extension = request()->file('articleImage')->getClientOriginalExtension();

        // If the articles directory does not exist, create it.
        if (!Storage::disk('public')->exists('articles/'))
            Storage::disk('public')->makeDirectory('articles/');

        $fileName = $imageName . '.' . $extension;

        /* If an image with the same name already exists, delete it first. */
        if (Storage::disk('public')->exists($destinationPath . '/' . $imageName)) {
            Storage::disk('public')->delete($destinationPath . '/' . $imageName);
        }

        /* Save the image to public storage */
        $file->save(public_path($destinationPath . $fileName));

        return $fileName;
    }
}
