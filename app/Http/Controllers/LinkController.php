<?php

namespace App\Http\Controllers;

use App\Link;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class LinkController extends Controller
{
    /**
     * Store a newly created resource in storage.
     * We store the colours for each link in the data row of the link itself and not in the colours table.
     * We do this, because it would fill up the colours table unnecessarily and we would have to make changes to that table.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $this->validateInput();

        Link::create([
            'label' => $request->linkLabel,
            'link' => $request->linkLink,
            'colour_label' => $request->colourLabel,
            'colour_button' => $request->colourButton,
            'footer_id' => 1
        ]);

        return redirect()->back()->with('message', 'Link created successfully');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Link $link
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Link $link)
    {
        $this->validateInput();

        $link->update([
            'label' => $request->linkLabel,
            'link' => $request->linkLink,
            'colour_label' => $request->colourLabel,
            'colour_button' => $request->colourButton
        ]);

        return redirect()->back()->with('message', 'Link updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Link $link
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Link $link)
    {
        try {
            $link->delete();
        } catch (\Exception $e) {
            return redirect()->back()->withErrors('Something went wrong! ' . $e);
        }

        return redirect()->back()->with('message', 'Language deleted successfully');
    }

    private function validateInput()
    {
        /* The colours must all be a 6 character HEX codes (#000000).
        Because the colour picker on the webpage does not recognize other colour code formats. */
        $rules = [
            'linkLabel' => ['required', 'max:255'],
            'linkLink' => ['required', 'max:255', 'active_url'],
            'colourLabel' => ['required', 'regex:/^(\#[\da-f]{6})$/'],
            'colourButton' => ['required', 'regex:/^(\#[\da-f]{6})$/']
        ];

        $v = Validator::make(request()->all(), $rules);
        $v->validate();
    }
}
