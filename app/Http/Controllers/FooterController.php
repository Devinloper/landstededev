<?php

namespace App\Http\Controllers;

use App\Footer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class FooterController extends Controller
{
    /**
     * Gets the footer data from the database to be viewed and edited in the admin panel.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function index()
    {
        $footer = Footer::first();
        return view('admin.footer', compact(['footer']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Footer  $footer
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Footer $footer)
    {
        $this->validateInput();

        $footer->update([
            'title' => $request->footerTitle,
            'content' => $request->footerContent
        ]);

        return redirect()->back()->with('message', 'Footer Content updated successfully');
    }

    /* Validate the text input */
    private function validateInput()
    {
        $rules = [
            'footerTitle' => ['required', 'max:255'],
            'footerContent' => ['required']
        ];

        $v = Validator::make(request()->all(), $rules);
        $v->validate();
    }
}
